export interface ProductInterface {
    id: number;
    image: string;
    title: string;
    price: number;
    description: string;
}